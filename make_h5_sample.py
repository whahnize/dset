import os
import provider

################### SAMPLE: image to h5file ###################
# Data directory
DATA_DIR = './sample_data'
TRAIN_DIR = os.path.join(DATA_DIR, 'train')

IM_DIM = 256
NUM_CLASSES = 2
dsets= {'images': [(IM_DIM, IM_DIM,3), 'uint8'], 
        'labels': [(NUM_CLASSES, ), 'uint8'],
        'uids': [(1, ), 'uint8']

        ### Add more dictionaries as you need ###
        # dataset_name: [data_shape, dtype]
        }
labels = ['D', 'S']
# labels = ['original', 'manipulated']

# Set dataset parameters
handle = provider.H5handler(dsets, labels)

# Create h5 with image directory and output path
handle.create_h5(TRAIN_DIR, os.path.join(DATA_DIR, 'train.h5'), shuffle=True)

# Split h5 with specified number. It will produce txt file which contains list of splitted h5 files
handle.split_h5(10, rm=False)

