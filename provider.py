import os
from PIL import Image
import numpy as np
import h5py
import random

class H5handler:
    def __init__(self, dsets, labels):
        self.dsets = dsets
        self.labels = labels
        self.num_classes = len(labels)
        self._labels_to_vectors()

    @staticmethod
    def get_data_files(list_filename):
        return [line.rstrip() for line in open(list_filename)]

    def create_h5(self, im_dir, output_path, **kwargs):
        self.h5file_path = output_path
        self.h5file = h5py.File(self.h5file_path, 'w')
        
        # All files of im_dir should be image
        files = [f for f in os.listdir(im_dir) if os.path.isfile(os.path.join(im_dir, f))]
        num_data = len(files)

        # Shuffle files if necessary
        if 'shuffle' in kwargs.keys() and kwargs['shuffle']==True:
            random.shuffle(files)

        # Create dataset
        for d_name, d_attr in self.dsets.items():
            self.h5file.create_dataset(d_name, (num_data,) + d_attr[0], dtype = d_attr[1])

        # Add data one by one
        for im_idx in range(num_data):
            self._append_data(im_idx, os.path.join(im_dir,files[im_idx]))

        self.h5file.close()

    def split_h5(self, num_per_split, **kwargs):
        self.h5file = h5py.File(self.h5file_path, 'r')
        num_entries = len(self.h5file[list(self.h5file.keys())[0]])

        for each_h5 in range(0, num_entries // num_per_split+1):

            with h5py.File(os.path.splitext(self.h5file_path)[0] + str(each_h5) + '.h5', 'w') as splt_h5file:
                start_idx = each_h5*num_per_split
                if each_h5== num_entries // num_per_split: end_idx = -1 # For last splitted dataset
                else: end_idx = (each_h5+1)*num_per_split

                # Copy dataset
                for d_name in self.dsets.keys():
                    splt_h5file.create_dataset(d_name, data = self.h5file[d_name][start_idx:end_idx])

        # Write names of splitted file as txt
        self.split_txt_path = os.path.splitext(self.h5file_path)[0]+'_files.txt'
        with open(self.split_txt_path,'w') as f:
            for each_hfds in range(0,num_entries // num_per_split+1):
                f.write(os.path.normcase(os.path.splitext(self.h5file_path)[0]+str(each_hfds)+'.h5\n'))
        self.h5file.close()
       
        if 'rm' in kwargs.keys() and kwargs['rm']==True:
            os.remove(self.h5file_path)
            del(self.h5file)
            del(self.h5file_path)
             
    def _labels_to_vectors(self):
        self.labels_to_vectors = {}
        vectors = list(np.array(np.eye(self.num_classes), dtype='uint8'))
        labels_and_vectors = zip(self.labels, vectors)
        for l, v in labels_and_vectors:
            self.labels_to_vectors[l] = v.reshape(1, self.num_classes)

    def _append_data(self, im_idx, path_to_img):
        filename = os.path.splitext(os.path.basename(path_to_img))[0]

        # Store image
        im = Image.open(path_to_img)
        self.h5file['images'][im_idx, ...] = np.array(im, dtype= self.h5file['images'].dtype)

        # Store other image-related information
        for dset, info in zip(list(self.dsets.keys())[1:], self._extract_info(filename)):
            self.h5file[dset][im_idx, ...] = info
    
    def _extract_info(self, filename):
        ### Modify below to your filename policy ###
        # example: 'D_000006_93_383'
        label, uid, _, _= filename.split('_')

        # Return should be aligned to dset key order
        # ex) label_vector, ...
        # return [label_vector, uid]
        return [self.labels_to_vectors[label], int(uid)]

