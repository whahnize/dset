import h5py
import os
import provider
import numpy as np 

# Data directory
DATA_DIR = './sample_data'

# Direct access for non-splitted .h5 file
h5file_path = os.path.join(DATA_DIR, 'train.h5')
h5file = h5py.File(h5file_path,'r')
h5file['images'], h5file['labels'], h5file['uids']

# Indirect access for splitted .h5 files with shuffling
H5_FILES = provider.H5handler.get_data_files( \
    os.path.join(DATA_DIR, 'train_files.txt'))

# Shuffle idx
file_idxs = np.arange(0, len(H5_FILES))
np.random.shuffle(file_idxs)

for fn in range(len(H5_FILES)):
    with h5py.File(H5_FILES[file_idxs[fn]],'r') as h5file:
        h5file['images']
        h5file['labels']
        h5file['uids']